<h1>{cart_lbl}</h1>
<div class="checkout">
    <div class="row header">
        <div class="col-sm-4 col-xs-6">
            <p class="header">{product_name_lbl}</p>
        </div>
        <div class="col-sm-3 hidden-xs product_price">
            <p class="header">Produkt pris</p>
        </div>
        <div class="col-sm-2 hidden-xs">
            <p class="header">{quantity_lbl}</p>
        </div>
        <div class="col-sm-3 col-xs-6 product_price">
            <p class="header">Samlet produkt pris</p>
        </div>
    </div>
    <div class="row">
        <!-- {product_loop_start} -->
        <div class="product-line">
            <div class="col-sm-3 col-xs-6">
                <div class="cartproducttitle">{product_name}</div>
                <div class="cartattribut">{product_attribute}</div>
                <div class="cartaccessory">{product_accessory}</div>
                <div class="cartwrapper">{product_wrapper}</div>
                <div class="cartuserfields">{product_userfields}</div>
                <div>{attribute_change}</div>
            </div>
            <div class="col-sm-1 hidden-xs">{product_thumb_image}</div>
            <div class="col-sm-3 hidden-xs">{product_price}</div>
            <div class="col-sm-2 hidden-xs">{update_cart}{remove_product}</div>
            <div class="col-sm-3 col-xs-6">{product_total_price}</div>
            <div class="clearfix"></div>
        </div>
        <!-- {product_loop_end} -->
    </div>
    <div class="row">
        <div class="col-sm-12 update">
            <span>{update}</span>
            <span>{empty_cart}</span>
        </div>
        <div class="totalprice-area">
            <div class="col-sm-4 discount-form">
                <p>
                    {discount_form_lbl}<p class="header">Indtast rabatkode:</p>
                {discount_form}
                </p>
            </div>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-5 col-sm-offset-7">
                        <p class="pull-left header">{product_subtotal_lbl}:</p>
                        <p class="pull-right">{product_subtotal_excl_vat}</p>
                    </div>
                    <div class="clearfix"></div>
                    <!-- {if discount}-->
                    <div class="col-sm-5 col-md-offset-7">
                        <p class="pull-left header">{discount_lbl}:</p>
                        <p class="pull-right">{discount}</p>
                    </div>
                    <div class="clearfix"></div>
                    <!-- {discount end if} -->
                    <!-- {if vat} -->
                    <div class="col-sm-5 col-sm-offset-7">
                        <p class="pull-left header">Moms:</p>
                        <p class="pull-right">{tax}</p>
                    </div>
                    <div class="clearfix"></div>
                    <!-- {vat end if} -->
                    <div class="col-sm-5 col-sm-offset-7">
                        <p class="pull-left header">Forsendelse:</p>
                        <p class="pull-right">{shipping_excl_vat}</p>
                    </div>
                    <div class="clearfix"></div>
                    <!-- {if payment_discount}-->
                    <div class="col-sm-5 col-sm-offset-7">
                        <p class="pull-left header">{payment_discount_lbl}:</p>
                        <p class="pull-right">{payment_order_discount}</p>
                    </div>
                    <div class="clearfix"></div>
                    <!-- {payment_discount end if}-->
                    <div class="col-sm-5 col-sm-offset-7">
                        <p class="pull-left header">{total_lbl}:</p>
                        <p class="pull-right">{total}</p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row">
                    <div class="col-xs-6 col-sm-2 col-sm-offset-7">{shop_more}</div>
                    <div class="col-xs-6 col-sm-3">{checkout_button}</div>
                </div>
            </div>
        </div>
    </div>
</div>