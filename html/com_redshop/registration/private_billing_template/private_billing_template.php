<table class="admintable" border="0">
    <tbody>
    <tr>

        <td> <label>{email_lbl}</label>{email}</td>
        <td><span class="required">*</span></td>
    </tr><!-- {retype_email_start} -->
    <tr>

        <td> <label>{retype_email_lbl}</label>{retype_email}</td>
        <td><span class="required">*</span></td>
    </tr><!-- {retype_email_end} -->
    <tr>

        <td><label>{firstname_lbl}</label>{firstname}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr>

        <td><label>{lastname_lbl}</label>{lastname}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr>

        <td><label>{address_lbl}</label>{address}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr>

        <td> <label>{zipcode_lbl}</label>{zipcode}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr>

        <td><label>{city_lbl}</label>{city}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr id="{country_txtid}" style="{country_style}">

        <td> <label>{country_lbl}</label>{country}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr id="{state_txtid}" style="{state_style}">

        <td><label>{state_lbl}</label>{state}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr>

        <td><label>{phone_lbl}</label>{phone}</td>
        <td><span class="required">*</span></td>
    </tr>
    <tr>
        <td colspan="2" class="extra">{private_extrafield}</td>
    </tr>
    </tbody>
</table>